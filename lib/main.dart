import 'package:flutter/material.dart';
import 'package:mg_draw/drawing_page.dart';


void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MG Draw',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DrawingPage(),
    );
  }
}


















































//Reference
//https://www.raywenderlich.com/25237210-building-a-drawing-app-in-flutter#toc-anchor-008